# About
This is a minimalistic administration interface for the [Octopress blogging
framework] [1]. It is written in python, using [Flask, a python web framework]
[2].


# Requirements
[Python3] [3]
[flask python module] [2]
[yaml python module] [4]


# Installation


[1]: http://octopress.org/ "Octopress"
[2]: http://flask.pocoo.org/ "Flask"
[3]: http://www.python.org/ "Python"
[4]: http://pyyaml.org/ "Pyyaml"
