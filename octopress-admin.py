#!/usr/bin/env python3
import yaml
import os
import re
from flask import Flask, render_template, request
from datetime import date
from subprocess import call, check_call, CalledProcessError, TimeoutExpired

ADMIN_CONF = yaml.load(open('config.yml'))
BLOG_PATH = ADMIN_CONF['blog_path']
blog_config_file = os.path.join(BLOG_PATH, '_config.yml')
BLOG_CONF = yaml.load(open(blog_config_file))
POSTS = {}

app = Flask(__name__)


# Seen on StackOverflow: http://stackoverflow.com/a/13197763/1112326
class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


class InvalidPost(Exception):
    def __init__(self, faulty_part, notes=''):
        self.faulty_part = faulty_part
        self.notes = notes


def parse_post_metadata(post_file):
    post = open(post_file)
    metadatas = {}

    metadata_lines = []
    in_metada = False
    for line in post:
        if line == '---\n' and not in_metada:
            in_metada = True
        elif line == '---\n' and in_metada:
            in_metada = False
            break
        elif in_metada:
            metadata_lines.append(line)
        else:
            pass  # Should not append, but let's assume it's not a problem

    if in_metada:
        metadatas = {'error': InvalidPost('Endless metadas')}
    else:
        try:
            metadatas = yaml.load('\n'.join(metadata_lines))
            metadatas['error'] = None

            # Determining permalink
            # TODO: support and use the permalink parameter in the blog's
            # configuration. And before that, find a better way to format the
            # title
            if isinstance(metadatas['date'], date):
                date_fmt = metadatas['date'].strftime('%Y/%m/%d/')
            else:
                date_fmt = metadatas['date'].split(' ')[0].replace('-', '/') + '/'
            title_fmt = ''.join([c for c in re.sub('-+', '-', metadatas['title'].replace(' ', '-')).lower() if c.isalnum() or c == '-'])
            dev_adress = ADMIN_CONF['dev_adress'] + '/'
            prod_adress = ADMIN_CONF['prod_adress'] + '/'

            metadatas['dev_permalink'] = dev_adress + 'blog/' + date_fmt + title_fmt
            metadatas['prod_permalink'] = prod_adress + 'blog/' + date_fmt + title_fmt
        except yaml.YAMLError as exc:
            metadatas = {'error': exc}

    metadatas['file'] = post_file
    return metadatas


def load_posts():
    posts = []
    for root, _, posts_path in os.walk(os.path.join(BLOG_PATH, 'source', '_posts')):
        for post_path in posts_path:
            post = parse_post_metadata(os.path.join(root, post_path))
            post['trashed'] = False
            posts.append(post)
    for root, _, posts_path in os.walk(os.path.join(BLOG_PATH, 'trash', '_posts')):
        for post_path in posts_path:
            post = parse_post_metadata(os.path.join(root, post_path))
            post['trashed'] = True
            posts.append(post)
    return posts


def new_post(post_name):
    # Fuck you rake https://github.com/imathis/octopress/issues/910
    if ',' in post_name:
        return ['Error: do not use commas in post name, you can change the title later while editing the post']
    rake_arg = 'new_post[' + post_name + ']'
    err = []
    with cd(BLOG_PATH):
        try:
            check_call(['bundle', 'exec', 'rake', rake_arg], timeout=3)
        except CalledProcessError as exc:
            err.append("Command `{}` failed with code {} and message:\n{}".format(exc.cmd, exc.returncode, exc.output))
        except TimeoutExpired as exc:
            err.append("Error: New post creation (" + post_name + ") has timout, there is probably already a post with this title")
    return err


@app.route('/trash_post/<post_name>')
def trash_post(post_name):
    posts = load_posts()

    post = [p for p in posts if p['title'] == post_name][0]
    error = ''

    if post['trashed']:
        error = 'This post (' + post['title'] + ') was already trashed'
    else:
        with cd(BLOG_PATH):
            call(['mv', post['file'], os.path.join('trash', '_posts')])
            post['trashed'] = True

    return admin_home(errors=[error])


@app.route('/recover_post/<post_name>')
def recover_post(post_name):
    posts = load_posts()

    post = [p for p in posts if p['title'] == post_name][0]
    error = ''

    if not post['trashed']:
        error = 'This post (' + post['title'] + ') was not trashed'
    else:
        with cd(BLOG_PATH):
            call(['mv', post['file'], os.path.join('source', '_posts')])
            post['trashed'] = False

    return admin_home(errors=[error])


@app.route('/delete_post/<post_name>')
def delete_post(post_name):
    posts = load_posts()
    err = ''
    try:
        post = [p for p in posts if p['title'] == post_name][0]
        os.remove(post['file'])
    except IndexError:
        err = 'This post (' + post_name + ') doesn\'t exists'
    except KeyError:  # TODO: fix that, that's lame
        err = 'Error: Cannot delete posts when some titles are faulty, contact your admin'
    return admin_home(errors=[err])


@app.route('/', methods=['GET', 'POST'])
def admin_home(errors=[]):
    posts = load_posts()

    if request.method == 'POST':
        errors = errors + new_post(request.form['new_post_name'])
        posts = load_posts()

    try:
        title_list = [p['title'] for p in posts if not p['trashed']]
        if len(title_list) != len(set(title_list)):  # TODO: support this case
            errors.append("Warning: two non-trashed posts have the same title")
    except KeyError:
        errors.append('Error: some posts are faulty')

    return render_template('index.html', blog=BLOG_CONF, posts=posts, errors=errors)


if __name__ == '__main__':
    app.debug = True
    app.run()
